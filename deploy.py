#!/usr/bin/env python

import shutil
import time
import os
import uuid
import tarfile
import logging
import subprocess
import urllib2

# defaults
default_log_level="info"
default_hc_retries=20
default_hc_sleep_interval=1 # second
default_artifact_download_url="https://s3.eu-central-1.amazonaws.com/devops-exercise/pandapics.tar.gz"
default_artifact_archive_name="pandapics.tar.gz"
default_base_download_path="/tmp/bigpanda_deployer/"
default_remove_images_directory=True # What to do if the images directory exists
default_hc_url="http://localhost:3000/health"

logger=None

# some boilerplate taken from python docs..
def logger_init(log_level):
    logger = logging.getLogger("deployer")
    level = logging.getLevelName(log_level.upper())
    logger.setLevel(level)
    ch = logging.StreamHandler()
    ch.setLevel(level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger

def deploy():
    start_time = time.time()

    # Init some values from defaults / environment
    log_level = os.getenv("BP_LOG_LEVEL", default_log_level)
    artifact_download_url = os.getenv("BP_ARTIFACT_DOWNLOAD_URL", default_artifact_download_url)
    remove_images_directory = os.getenv("BP_REMOVE_IMAGES_DIRECTORY", default_remove_images_directory)
    max_tries = os.getenv("BP_HC_MAX_TRIES", default_hc_retries)
    hc_url = os.getenv("BP_HC_URL", default_hc_url)
    sleep_interval = os.getenv("BP_HC_SLEEP_INTERVAL", default_hc_sleep_interval)
    download_base_path = os.getenv("BP_DOWNLOAD_BASE_PATH", default_base_download_path)

    global logger
    logger = logger_init(log_level)

    logger.info("[main] Starting deployment..")
    logger.info("[main] log level: {}".format(log_level))
    logger.info("[main] artifact_download_url: {}".format(artifact_download_url))
    logger.info("[main] hc url: {} ".format(hc_url))
    logger.info("[main] hc sleep interval: {} seconds".format(sleep_interval))
    logger.info("[main] hc max retries: {} times".format(max_tries))
    logger.info("[main] download base path: {}".format(download_base_path))

    artfiact_phase_success = get_artifacts(download_base_path, artifact_download_url, remove_images_directory)
    if not artfiact_phase_success:
        logger.error("[main] artifact phase filed, rolling back from that point..")
        exit(1)

    docker_phase_success = run_containers()
    if not docker_phase_success:
        logger.error("[main] docker phase filed, rolling back from that point..")
        exit(1)

    deploy_success = probe_health(max_tries=max_tries, sleep_interval=sleep_interval, healthcheck_url=hc_url)

    end_time = time.time()
    deployment_elapsed = end_time - start_time
    logger.info("[main] Deployment finished with success: [{}] after {} seconds".format(deploy_success, deployment_elapsed))

def run_containers():
    logger.info("[docker-phase] Stopping old versions of the containers..")
    fnull = open(os.devnull, 'w')
    ret = subprocess.check_call(["docker-compose", "stop"], stdout=fnull,stderr=fnull)
    if ret != 0:
        fnull.close()
        return False

    # forcing the build of new images due to the fact that the public/images file are being baked in and not mounted from storage or something else..
    logger.info("[docker-phase] Forcing building new images...")
    ret = subprocess.check_call(["docker-compose", "build"], stdout=fnull,stderr=fnull)
    if ret != 0:
        fnull.close()
        return False

    logger.info("[docker-phase] Going to run [docker-compose up]")
    ret = subprocess.check_call(["docker-compose", "up", "-d"],stdout=fnull,stderr=fnull)
    if ret != 0:
        fnull.close()
        return False

    logger.info("[docker-phase] Finished with docker-compose")
    fnull.close()
    return True

def download_artifacts(**kwargs):
    try:
        download_url , download_dir, filename = kwargs['download_url'], kwargs['download_dir'], kwargs['filename']
        res = urllib2.urlopen(download_url)
        content = res.read()
        archive_file = open(os.path.join(download_dir, filename), "w+")
        with open(os.path.join(download_dir, filename)):
            archive_file.write(content)
        return True
    except Exception as e:
        logger.error("[artifact-phase]  download failed: {}".format(e))
        return False

def extract_archive(**kwargs):
    try: 
        tf = tarfile.open(kwargs['archive_path'])
        tf.extractall(kwargs['extract_path'])
        tf.close()
        return True
    except Exception as e:
        logger.error("[artifact-phase] untar file failed: {}".format(e))
        return False

def get_artifacts(download_base_path, artifact_download_url, delete_on_exists):
    # generating unique tmp download directory on each deploy
    download_dir = os.path.join(download_base_path, str(uuid.uuid4()))
    logger.info("[artifact-phase] Downloading dir is: {}".format(download_dir))
    os.makedirs(download_dir, 0755)

    images_dir = os.path.join(os.getcwd(), "public/images")
    images_dir_exists = os.path.exists(images_dir)

    if images_dir_exists:
        if delete_on_exists:
            logger.warn("[artifact-phase] Going to delete older /public/images directory")
            shutil.rmtree(images_dir, ignore_errors=True)
        else:
            # failing deployment as expected
            logger.error("[artifact-phase] path {} already exists and delete_on_exists flag is False".format(images_dir))
            return False

    logger.debug("[artifact-phase] Starting with download to: {}".format(download_dir))
    download_success = download_artifacts(
        download_url=artifact_download_url, 
        download_dir=download_dir, 
        filename=default_artifact_archive_name)
    logger.debug("[artifact-phase] Finished with download with success: {}..".format(download_success))
    if not download_success:
        logger.error("[artifact-phase] download failed, won't proceed to untar..")
        return False

    logger.debug("[artifact-phase] Starting with extracting..")
    images_archive_path = os.path.join(download_dir, default_artifact_archive_name)
    extract_success = extract_archive(archive_path=images_archive_path, extract_path=images_dir)
    logger.debug("[artifact-phaes] delete tmp download dir: {}".format(download_dir))
    shutil.rmtree(download_dir, ignore_errors=True)
    logger.debug("[artifact-phase] Finished extracting with success: {} ..".format(extract_success))

    return extract_success

def probe_health(**kwargs):
    max_tries, sleep_interval, hc_url = kwargs['max_tries'], kwargs['sleep_interval'], kwargs['healthcheck_url']
    # could be a better way like exp. backoff..
    for tries in xrange(0, max_tries):
        time.sleep(sleep_interval)
        try:
            logger.info("[hc] health probing try: {}..".format(tries))
            # TODO: add timeout for request
            response = urllib2.urlopen(hc_url)
            status_code = response.getcode()
            response.close()
            if status_code == 200:
                logger.info("[hc] We are healthy..")
                return True
        except Exception as e:
            # urllib2 return exceptin for 500 so we are taking care of it here
            logger.warn("[hc] health check failed: {}".format(e))
        finally:
            logger.info("[hc] sleeping for {} second before retrying...".format(sleep_interval))

    logger.error("[hc] Reached max tries of: {} for health check without being healthy...".format(max_tries))
    return False

if __name__ == "__main__":
    deploy()