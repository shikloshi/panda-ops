# Deployment

Provided files: 

- `docker-compose.yaml`for running app and db
- `deploy.py`python script to deploy app and db

## Run deployment

* Deployment python script was created and tested with `Python 2.7.15`

```sh
$ ./deploy.py
```

## Configuration

* Configuration is passed via environment variables

| envvar name  |  default value  | description  |
|---|---|---|---|---|
| `BP_LOG_LEVEL`  | info  |  log level: warn, info, debug, trace, error |
|  `BP_ARTIFACT_DOWNLOAD_URL` |  [This](https://s3.eu-central-1.amazonaws.com/devops-exercise/pandapics.tar.gz) | where to download images from | 
|  `BP_DOWNLOAD_BASE_PATH` | `/tmp/bigpanda`  |  prefix for where to download the images tar.gz |   |   |
|  `BP_REMOVE_IMAGES_DIRECTORY` | True |   |   |   |
|  `BP_HC_MAX_TRIES` | 20  | number of health check tries  | 
| `BP_HC_URL`  | `http://localhost:3000/health`  | url for health check  |   |   |
|  `BP_HC_SLEEP_INTERVAL` |  1 second | how many seconds to wait before health check tries  |   |   |